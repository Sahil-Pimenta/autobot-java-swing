-- MySQL dump 10.10
--
-- Host: localhost    Database: autobot
-- ------------------------------------------------------
-- Server version	5.0.27-community-nt

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appointdb`
--

DROP TABLE IF EXISTS `appointdb`;
CREATE TABLE `appointdb` (
  `AutoID` bigint(20) NOT NULL,
  `Name` varchar(100) default NULL,
  `Address` varchar(250) default NULL,
  `Brand` varchar(25) default NULL,
  `Model` varchar(25) default NULL,
  `ContactNo` bigint(20) default NULL,
  `Issue1` varchar(35) default NULL,
  `Issue2` varchar(35) default NULL,
  `TotalAmount` bigint(20) default NULL,
  `Date` varchar(250) default NULL,
  PRIMARY KEY  (`AutoID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `appointdb`
--

LOCK TABLES `appointdb` WRITE;
/*!40000 ALTER TABLE `appointdb` DISABLE KEYS */;
INSERT INTO `appointdb` VALUES (1,'Sahil Pimenta','Pitambar Villa,  Khopat, Thane','Hyundai','Santro',9004123321,'Fuse Check-Up','Coolant Replacement',4000,'Sat Jan 28 03:35:01 IST 2034'),(2,'Ajay','Pimenta house, Cherai, Thane','Hyundai','Santro',9874565478,'Full Servicing','Battery Check-Up',7500,'Fri Jan 13 03:36:06 IST 2034'),(5,'Rahul Sharma','Pitambar villa, thane west','Hyundai','Santro',9658741235,'Full Servicing','Coolant Replacement',6500,'Fri Jan 20 05:09:04 IST 2034'),(6,'Sahil Pimenta','Pimenta house, thane','Hyundai','Santro',9632514785,'Full Servicing','Fuse Check-Up',7500,'Tue Jan 31 05:26:38 IST 2017');
/*!40000 ALTER TABLE `appointdb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `autocust`
--

DROP TABLE IF EXISTS `autocust`;
CREATE TABLE `autocust` (
  `AutoID` bigint(20) NOT NULL,
  `Name` varchar(100) default NULL,
  `Address` varchar(250) default NULL,
  `Vehicle` varchar(35) default NULL,
  `Brand` varchar(25) default NULL,
  `Model` varchar(25) default NULL,
  `ContactNo` bigint(20) default NULL,
  `Pass` varchar(100) default NULL,
  PRIMARY KEY  (`AutoID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `autocust`
--

LOCK TABLES `autocust` WRITE;
/*!40000 ALTER TABLE `autocust` DISABLE KEYS */;
INSERT INTO `autocust` VALUES (1,'Sahil Pimenta','Pitambar Villa,  Khopat, Thane','Bike','Paggio','Vespa',9004123321,'cms@123'),(2,'Ajay','Pimenta house, Cherai, Thane','Car','Hyundai','Santro',9874565478,'cms'),(3,'Rohan Joshi','Bhaskar Colony, Thane','Car','Hyundai','Santro',8745125478,'helloworld'),(4,'Ram Kolhi','Louis wadi, Thane west','Car','Maruti','Swift',9874445214,'helloapple'),(5,'Rahul Sharma','Pitambar villa, thane west','Car','Hyundai','Santro',9658741235,'hello'),(6,'Sahil Pimenta','Pimenta house, thane','Car','Hyundai','Santro',9632514785,'cms12345');
/*!40000 ALTER TABLE `autocust` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bikedb`
--

DROP TABLE IF EXISTS `bikedb`;
CREATE TABLE `bikedb` (
  `Brand` varchar(15) default NULL,
  `Model` varchar(15) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bikedb`
--

LOCK TABLES `bikedb` WRITE;
/*!40000 ALTER TABLE `bikedb` DISABLE KEYS */;
INSERT INTO `bikedb` VALUES ('Bajaj','Pulsar'),('Honda','Activa'),('Paggio','Vespa'),('TVS','Jupiter'),('TVS','Pep'),('Honda','Activa i'),('Triumph','Street Twin'),('Triumph','Bonneville'),('Triumph','Thruxton');
/*!40000 ALTER TABLE `bikedb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cardb`
--

DROP TABLE IF EXISTS `cardb`;
CREATE TABLE `cardb` (
  `Brand` varchar(15) default NULL,
  `Model` varchar(15) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cardb`
--

LOCK TABLES `cardb` WRITE;
/*!40000 ALTER TABLE `cardb` DISABLE KEYS */;
INSERT INTO `cardb` VALUES ('Hyundai','Santro'),('Hyundai','i20'),('Hyundai','i10'),('Maruti','800'),('Maruti','Swift'),('Skoda','Octavia'),('Skoda','Fabia');
/*!40000 ALTER TABLE `cardb` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dev`
--

DROP TABLE IF EXISTS `dev`;
CREATE TABLE `dev` (
  `DevID` bigint(20) NOT NULL,
  `DevName` varchar(20) default NULL,
  `DevPass` varchar(20) default NULL,
  PRIMARY KEY  (`DevID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dev`
--

LOCK TABLES `dev` WRITE;
/*!40000 ALTER TABLE `dev` DISABLE KEYS */;
INSERT INTO `dev` VALUES (1,'Ramesh','autobot123'),(2,'Laukik','autobot1234'),(3,'Abhishek','autobot12345'),(4,'Raunak','autobot123456'),(5,'Karen','autobot1234567'),(6,'Aditi','autobot12345678');
/*!40000 ALTER TABLE `dev` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `issuedb`
--

DROP TABLE IF EXISTS `issuedb`;
CREATE TABLE `issuedb` (
  `Issue` varchar(20) default NULL,
  `Amount` int(11) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `issuedb`
--

LOCK TABLES `issuedb` WRITE;
/*!40000 ALTER TABLE `issuedb` DISABLE KEYS */;
INSERT INTO `issuedb` VALUES ('<none>',0),('Full Servicing',5000),('Fuse Check-Up',2500),('Battery Check-Up',2500),('Coolant Replacement',1500),('Dent Fixing',1700);
/*!40000 ALTER TABLE `issuedb` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-02-15  7:12:31
