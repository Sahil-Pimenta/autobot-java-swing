/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaminiprojectautobot;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Sahil
 */
@Entity
@Table(name = "autocust", catalog = "autobot", schema = "")
@NamedQueries({
    @NamedQuery(name = "Autocust.findAll", query = "SELECT a FROM Autocust a"),
    @NamedQuery(name = "Autocust.findByAutoID", query = "SELECT a FROM Autocust a WHERE a.autoID = :autoID"),
    @NamedQuery(name = "Autocust.findByName", query = "SELECT a FROM Autocust a WHERE a.name = :name"),
    @NamedQuery(name = "Autocust.findByAddress", query = "SELECT a FROM Autocust a WHERE a.address = :address"),
    @NamedQuery(name = "Autocust.findByVehicle", query = "SELECT a FROM Autocust a WHERE a.vehicle = :vehicle"),
    @NamedQuery(name = "Autocust.findByBrand", query = "SELECT a FROM Autocust a WHERE a.brand = :brand"),
    @NamedQuery(name = "Autocust.findByModel", query = "SELECT a FROM Autocust a WHERE a.model = :model"),
    @NamedQuery(name = "Autocust.findByContactNo", query = "SELECT a FROM Autocust a WHERE a.contactNo = :contactNo"),
    @NamedQuery(name = "Autocust.findByPass", query = "SELECT a FROM Autocust a WHERE a.pass = :pass")})
public class Autocust implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "AutoID")
    private Long autoID;
    @Column(name = "Name")
    private String name;
    @Column(name = "Address")
    private String address;
    @Column(name = "Vehicle")
    private String vehicle;
    @Column(name = "Brand")
    private String brand;
    @Column(name = "Model")
    private String model;
    @Column(name = "ContactNo")
    private BigInteger contactNo;
    @Column(name = "Pass")
    private String pass;

    public Autocust() {
    }

    public Autocust(Long autoID) {
        this.autoID = autoID;
    }

    public Long getAutoID() {
        return autoID;
    }

    public void setAutoID(Long autoID) {
        Long oldAutoID = this.autoID;
        this.autoID = autoID;
        changeSupport.firePropertyChange("autoID", oldAutoID, autoID);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        String oldAddress = this.address;
        this.address = address;
        changeSupport.firePropertyChange("address", oldAddress, address);
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        String oldVehicle = this.vehicle;
        this.vehicle = vehicle;
        changeSupport.firePropertyChange("vehicle", oldVehicle, vehicle);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        String oldBrand = this.brand;
        this.brand = brand;
        changeSupport.firePropertyChange("brand", oldBrand, brand);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        String oldModel = this.model;
        this.model = model;
        changeSupport.firePropertyChange("model", oldModel, model);
    }

    public BigInteger getContactNo() {
        return contactNo;
    }

    public void setContactNo(BigInteger contactNo) {
        BigInteger oldContactNo = this.contactNo;
        this.contactNo = contactNo;
        changeSupport.firePropertyChange("contactNo", oldContactNo, contactNo);
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        String oldPass = this.pass;
        this.pass = pass;
        changeSupport.firePropertyChange("pass", oldPass, pass);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (autoID != null ? autoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autocust)) {
            return false;
        }
        Autocust other = (Autocust) object;
        if ((this.autoID == null && other.autoID != null) || (this.autoID != null && !this.autoID.equals(other.autoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaminiprojectautobot.Autocust[ autoID=" + autoID + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
