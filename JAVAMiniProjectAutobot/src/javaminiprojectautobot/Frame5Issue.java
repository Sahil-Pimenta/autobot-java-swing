package javaminiprojectautobot;
import java.sql.*;



public class Frame5Issue extends javax.swing.JFrame {
    Connection con;
    ResultSet rs,rs1,rs2;
    Statement st,st1,st2;
    String AutoID1,CustName1,Brand1,Model1,ContactNo1,Address1;
   public Frame5Issue()
   {
       initComponents();
       
       
   }
        public Frame5Issue(String AutoID,String Cust,String brand,String model,String contact,String address)
        {
            initComponents();
            try
        {
            Class.forName("com.mysql.jdbc.Driver");
        }
        catch(ClassNotFoundException e)
        {
            System.out.println(e.toString());
        }
         try
        {
            con=DriverManager.getConnection("jdbc:mysql://localhost:3306/autobot","root","cms@123");
            st=con.createStatement();
            st1=con.createStatement();
            st2=con.createStatement();
            
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
        }
       try
         {
             String str2="select Issue from issuedb";
             rs=st.executeQuery(str2);
             IssueCombo.removeAllItems();
             while(rs.next())
            {
                IssueCombo.addItem(rs.getString(1));
            }
         }
         catch(SQLException exe)
         {
             System.out.println(exe.toString());
         }
         IssueCombo2.setVisible(false);
            AutoID1=AutoID;
            CustName1=Cust;
            Brand1=brand;
            Model1=model;
            ContactNo1=contact;
            Address1=address;
        }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jOptionPane1 = new javax.swing.JOptionPane();
        jOptionPane2 = new javax.swing.JOptionPane();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        ServiceButton = new javax.swing.JButton();
        BrandLabel = new javax.swing.JLabel();
        IssueCombo = new javax.swing.JComboBox();
        IssueCombo2 = new javax.swing.JComboBox<>();
        AddButton = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        TotalAmtLabel = new javax.swing.JLabel();
        DateField = new com.toedter.calendar.JDateChooser();
        BackButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                formMouseClicked(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel5.setText("Issue:");

        jLabel6.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel6.setText("Total Amount:");

        ServiceButton.setFont(new java.awt.Font("Trebuchet MS", 1, 24)); // NOI18N
        ServiceButton.setText("Service");
        ServiceButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ServiceButtonActionPerformed(evt);
            }
        });

        BrandLabel.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N

        IssueCombo.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        IssueCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IssueComboActionPerformed(evt);
            }
        });

        IssueCombo2.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        IssueCombo2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "none" }));
        IssueCombo2.setToolTipText("");
        IssueCombo2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                IssueCombo2ItemStateChanged(evt);
            }
        });
        IssueCombo2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                IssueCombo2ActionPerformed(evt);
            }
        });

        AddButton.setText("+");
        AddButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AddButtonActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N
        jLabel8.setText("Date:");

        TotalAmtLabel.setFont(new java.awt.Font("Trebuchet MS", 1, 18)); // NOI18N

        BackButton.setFont(new java.awt.Font("Trebuchet MS", 0, 18)); // NOI18N
        BackButton.setText("Back");
        BackButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                BackButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(jLabel8))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(IssueCombo, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(AddButton)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(TotalAmtLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(170, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(DateField, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(IssueCombo2, 0, 154, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(BrandLabel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(170, 170, 170)
                        .addComponent(ServiceButton))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(BackButton)))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(BackButton)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(IssueCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(AddButton, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(22, 22, 22)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(BrandLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(IssueCombo2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(51, 51, 51)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8)
                    .addComponent(DateField, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(87, 87, 87)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(TotalAmtLabel, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 49, Short.MAX_VALUE)
                .addComponent(ServiceButton, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void AddButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AddButtonActionPerformed
        // TODO add your handling code here:
        IssueCombo2.setVisible(true);
        try
        {
            String str3="select Issue from issuedb";
             rs2=st2.executeQuery(str3);
             IssueCombo2.removeAllItems();
             while(rs2.next())
            {
                IssueCombo2.addItem(rs2.getString(1));
            }
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
        }
        IssueCombo2.removeItem(IssueCombo.getSelectedItem());
        
    }//GEN-LAST:event_AddButtonActionPerformed

    private void IssueCombo2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IssueCombo2ActionPerformed
        // TODO add your handling code here:
        
        
        
    }//GEN-LAST:event_IssueCombo2ActionPerformed

    private void ServiceButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ServiceButtonActionPerformed
        // TODO add your handling code here:
        
        /*String day=DayCombo.getSelectedItem().toString();
        String month=MonthCombo.getSelectedItem().toString();
        String year=YearCombo.getSelectedItem().toString();
        String date=day+"/"+month+"/"+year;*/ 
        String date=DateField.getDate().toString();
        try
        {
            String str="insert into appointdb values("+AutoID1+",'"+CustName1+"','"+Address1+"','"+Brand1+"','"+Model1+"','"+ContactNo1+"','"+IssueCombo.getSelectedItem().toString()+"','"+IssueCombo2.getSelectedItem().toString()+"',"+TotalAmtLabel.getText()+",'"+date+"')";
            st.executeUpdate(str);
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
        }
        Frame6Invoice fr=new Frame6Invoice(AutoID1,CustName1,Model1,date,ContactNo1);
        fr.setVisible(true);
        this.setVisible(false);
        
       
    }//GEN-LAST:event_ServiceButtonActionPerformed

    private void IssueCombo2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_IssueCombo2ItemStateChanged
        // TODO add your handling code here:
        
    }//GEN-LAST:event_IssueCombo2ItemStateChanged

    private void IssueComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_IssueComboActionPerformed
        // TODO add your handling code here:
        IssueCombo2.setVisible(false);
        IssueCombo2.setSelectedItem("<none>");
    }//GEN-LAST:event_IssueComboActionPerformed

    private void formMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseClicked
        // TODO add your handling code here:
        try
        {
            int amt=0;
            String str="select Amount from issuedb where Issue='"+IssueCombo.getSelectedItem().toString()+"'";
            String str1="select Amount from issuedb where Issue='"+IssueCombo2.getSelectedItem().toString()+"'";
            rs=st.executeQuery(str);
            rs1=st1.executeQuery(str1);
            while(rs.next())
            {
                amt=amt+rs.getInt(1);
                TotalAmtLabel.setText(Integer.toString(amt));
            }
            while(rs1.next())
            {
                amt=amt+rs1.getInt(1);
                TotalAmtLabel.setText(Integer.toString(amt));
            }
        }
        catch(SQLException e)
        {
            System.out.println(e.toString());
        }
    }//GEN-LAST:event_formMouseClicked

    private void BackButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_BackButtonActionPerformed
        // TODO add your handling code here:
        Frame4Account h=new Frame4Account(AutoID1);
        h.setVisible(true);
        this.setVisible(false);
    }//GEN-LAST:event_BackButtonActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Frame5Issue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Frame5Issue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Frame5Issue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Frame5Issue.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Frame5Issue().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton AddButton;
    private javax.swing.JButton BackButton;
    private javax.swing.JLabel BrandLabel;
    private com.toedter.calendar.JDateChooser DateField;
    private javax.swing.JComboBox IssueCombo;
    private javax.swing.JComboBox<String> IssueCombo2;
    private javax.swing.JButton ServiceButton;
    private javax.swing.JLabel TotalAmtLabel;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JOptionPane jOptionPane1;
    private javax.swing.JOptionPane jOptionPane2;
    // End of variables declaration//GEN-END:variables
}
