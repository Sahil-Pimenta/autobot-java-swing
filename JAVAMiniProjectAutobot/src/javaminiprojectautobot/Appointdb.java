/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaminiprojectautobot;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.math.BigInteger;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Sahil
 */
@Entity
@Table(name = "appointdb", catalog = "autobot", schema = "")
@NamedQueries({
    @NamedQuery(name = "Appointdb.findAll", query = "SELECT a FROM Appointdb a"),
    @NamedQuery(name = "Appointdb.findByAutoID", query = "SELECT a FROM Appointdb a WHERE a.autoID = :autoID"),
    @NamedQuery(name = "Appointdb.findByName", query = "SELECT a FROM Appointdb a WHERE a.name = :name"),
    @NamedQuery(name = "Appointdb.findByAddress", query = "SELECT a FROM Appointdb a WHERE a.address = :address"),
    @NamedQuery(name = "Appointdb.findByBrand", query = "SELECT a FROM Appointdb a WHERE a.brand = :brand"),
    @NamedQuery(name = "Appointdb.findByModel", query = "SELECT a FROM Appointdb a WHERE a.model = :model"),
    @NamedQuery(name = "Appointdb.findByContactNo", query = "SELECT a FROM Appointdb a WHERE a.contactNo = :contactNo"),
    @NamedQuery(name = "Appointdb.findByIssue1", query = "SELECT a FROM Appointdb a WHERE a.issue1 = :issue1"),
    @NamedQuery(name = "Appointdb.findByIssue2", query = "SELECT a FROM Appointdb a WHERE a.issue2 = :issue2"),
    @NamedQuery(name = "Appointdb.findByTotalAmount", query = "SELECT a FROM Appointdb a WHERE a.totalAmount = :totalAmount"),
    @NamedQuery(name = "Appointdb.findByDate", query = "SELECT a FROM Appointdb a WHERE a.date = :date")})
public class Appointdb implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "AutoID")
    private Long autoID;
    @Column(name = "Name")
    private String name;
    @Column(name = "Address")
    private String address;
    @Column(name = "Brand")
    private String brand;
    @Column(name = "Model")
    private String model;
    @Column(name = "ContactNo")
    private BigInteger contactNo;
    @Column(name = "Issue1")
    private String issue1;
    @Column(name = "Issue2")
    private String issue2;
    @Column(name = "TotalAmount")
    private BigInteger totalAmount;
    @Column(name = "Date")
    private String date;

    public Appointdb() {
    }

    public Appointdb(Long autoID) {
        this.autoID = autoID;
    }

    public Long getAutoID() {
        return autoID;
    }

    public void setAutoID(Long autoID) {
        Long oldAutoID = this.autoID;
        this.autoID = autoID;
        changeSupport.firePropertyChange("autoID", oldAutoID, autoID);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        String oldAddress = this.address;
        this.address = address;
        changeSupport.firePropertyChange("address", oldAddress, address);
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        String oldBrand = this.brand;
        this.brand = brand;
        changeSupport.firePropertyChange("brand", oldBrand, brand);
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        String oldModel = this.model;
        this.model = model;
        changeSupport.firePropertyChange("model", oldModel, model);
    }

    public BigInteger getContactNo() {
        return contactNo;
    }

    public void setContactNo(BigInteger contactNo) {
        BigInteger oldContactNo = this.contactNo;
        this.contactNo = contactNo;
        changeSupport.firePropertyChange("contactNo", oldContactNo, contactNo);
    }

    public String getIssue1() {
        return issue1;
    }

    public void setIssue1(String issue1) {
        String oldIssue1 = this.issue1;
        this.issue1 = issue1;
        changeSupport.firePropertyChange("issue1", oldIssue1, issue1);
    }

    public String getIssue2() {
        return issue2;
    }

    public void setIssue2(String issue2) {
        String oldIssue2 = this.issue2;
        this.issue2 = issue2;
        changeSupport.firePropertyChange("issue2", oldIssue2, issue2);
    }

    public BigInteger getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(BigInteger totalAmount) {
        BigInteger oldTotalAmount = this.totalAmount;
        this.totalAmount = totalAmount;
        changeSupport.firePropertyChange("totalAmount", oldTotalAmount, totalAmount);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        String oldDate = this.date;
        this.date = date;
        changeSupport.firePropertyChange("date", oldDate, date);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (autoID != null ? autoID.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Appointdb)) {
            return false;
        }
        Appointdb other = (Appointdb) object;
        if ((this.autoID == null && other.autoID != null) || (this.autoID != null && !this.autoID.equals(other.autoID))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaminiprojectautobot.Appointdb[ autoID=" + autoID + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
